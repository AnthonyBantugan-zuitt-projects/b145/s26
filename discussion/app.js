db.users.insertONe({
	""
})

// Find Operation
db.collections.find({query}, {field projection})

// Query Operators

// Comparison Operators
// Syntax: { field: { $lt: value } }

// mini activity
// look for a document/s that has an age of less than 50
db.users.find({
	"age": {$lt: 50}
})

// look for a document/s that has an age of greater than or equal to 50
db.users.find({
	"age": {$gte: 50}
})
// returned 3 documents

// look for a document/s that has an age of not equal equal to 82
db.users.find({
	"age": {$ne: 50}
})
// returned 3 documents

// look for a document/s that has names hawking and doe
db.users.find({
	"lastName": { $in: ["Hawking", "Doe"]}
})
// returned 2 document

// look for a document/s that has courses HTML and React

db.users.find({
	"courses": { $in: ["HTML", "React"]}
})

// Logical Query Operator
// "OR" logical operator
// look for a document/s that has naem Niel or has an age of 25.
db.users.find( { $or: [{"firstName": "Neil"}, {"age": 25}]})

// look for a document/s that has naem Niel or has an age of greater than 30.
db.users.find({$or:[{"firstName": "Neil"}, {"age": {$gt: 30}}]})
// returned 3 documents

// "AND" logical operator

// look for a document/s that has name age of not equal to 82 and age not equal to 76.
db.users.find(
	{
		$and: [
			{"age": {$ne: 82}},
			{"age": {$ne: 76}}
		]
	}
	)
	
	
	
// Field Projection
	// allows us to include or exclude specific fields in a returning document

	db.users.find(
		{
			"firstName": "Jane"
		},
		{
			"firstName": 1,
			"lastName": 1,
			"contact": 1
		}
	)

	// look for a document/s that has a name hawking and exclude contact and department dields int he returning document
	db.users.find(
		{
			"lastName": "Hawking"
		},
		{
			"department": 0,
			"contact": 0
		}
	)
	db.users.find(
		{
			"firstName": "Neil"
		},
		{
			"id_": 0
			"firstName": 1,
			"lastName": 1,
			"contact": 1
		}
	)
	