// SOLUTION #2
db.users.find(
    {
        $or: [
            {
                "firstName": {$regex: "s", $options: "i"}
            },
            {
                "lastName": {$regex: "d", $options: "i"}
            }
        ]
    },
    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1
    }
);

// SOLUTION #3
db.users.find(
    {
        $and: [
            {"department": "HR"},
            {"age": {$gte: 70}}
        ]
    }
)

// SOLUTION #4
db.users.find(
    {
        $and: [
            {"firstName": {$regex: "e", $options: "i"}},
            {"age": {$lte: 30}}
        ]
    }
)